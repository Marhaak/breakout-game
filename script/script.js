var game = new Phaser.Game(480, 320, Phaser.AUTO, null, {
  preload: preload, create: create, update: update
});

// Variable for the ball.
var ball;

// Variable for the paddle.
var paddle;

// Hold our bricks.
var bricks;

// Used when we add a new brick.
var newBrick;

// Store our information about the bricks.
var brickInfo;

// Used to write the score on the screen.
var scoreText;

// Holding the score.
var score = 0;

// Hold the number of lifes you have left
var lives = 3;

// Used to write your life on the screen.
var livesText;

// Hold the text that will been shown to the player when he looses a life.
var lifeLostText;

// False/True if the game is playting or not.
var playing = false;

// The "Start Game" button.
var startButton;

// Font and style for the shown text.
textStyle = { font: '18px Arial', fill: '#0095DD' };

// This function deals with loading of the game.
function preload() {
	handleRemoteImagesOnJSFiddle();
	game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
	game.scale.pageAlighHorizontally = true;
	game.scale.pageAlignVertically = true;
	game.stage.backgroundColor = '#eee';
	game.load.image('paddle', 'img/paddle.png');
	game.load.image('brick', 'img/brick.png');
    game.load.spritesheet('ball', 'img/wobble.png', 20, 20);
    game.load.spritesheet('button', 'img/button.png', 120, 40);
}

// This function will run when the game is loaded.
function create() {
	// Starting the physics system.
	game.physics.startSystem(Phaser.Physics.ARCADE);

	// Adding the sprites to the world.
	ball = game.add.sprite(game.world.width*0.5, game.world.height-25, 'ball');
    ball.animations.add('wobble', [0,1,0,2,0,1,0,2,0], 24);
	paddle = game.add.sprite(game.world.width*0.5, game.world.height-5, 'paddle');
	ball.anchor.set(0.5);
	paddle.anchor.set(0.5, 1);
	game.physics.enable(paddle, Phaser.Physics.ARCADE);
	game.physics.enable(ball, Phaser.Physics.ARCADE);
	paddle.body.immovable = true;
	ball.body.collideWorldBounds = true;
	ball.body.bounce.set(1);

	game.physics.arcade.checkCollision.down = false;
	ball.checkWorldBounds = true;
	ball.events.onOutOfBounds.add(ballLeaveScreen, this);

	initBricks();

	scoreText = game.add.text(5,5, 'Points: 0', textStyle);
    livesText = game.add.text(game.world.width-5, 5, 'Lives: '+lives, textStyle);
    livesText.anchor.set(1,0);
    lifeLostText = game.add.text(game.world.width*0.5, game.world.height*0.5, 'Life lost, click to continue', textStyle);
    lifeLostText.anchor.set(0.5);
    lifeLostText.visible = false;
    startButton = game.add.button(game.world.width*0.5, game.world.height*0.5, 'button', startGame, this, 1,0,2);
    startButton.anchor.set(0.5);
  
}

// This is the tick function, runs every frame.
function update() {
	game.physics.arcade.collide(ball, paddle, ballHitPaddle);
	game.physics.arcade.collide(ball, bricks, ballHitBrick);
    if(playing) {
    	paddle.x = game.input.x || game.world.width*0.5;
    }
}


// this function take care of loading the images from the remote server
function handleRemoteImagesOnJSFiddle() {
	game.load.baseURL = 'https://end3r.github.io/Gamedev-Phaser-Content-Kit/demos/';
	game.load.crossOrigin = 'anonymous';
}

function initBricks() {
	brickInfo = {
		width: 50,
		height: 20,
		count: {
			row: 7,
			col: 3
		},
		offset: {
			top: 50,
			left: 60
		},
		padding: 10
	}

	bricks = game.add.group();

	for(i = 0; i < brickInfo.count.col; i++) {
		for(j = 0; j < brickInfo.count.row; j++) {
			// Create new brick and add it to the group.
			var brickX = (j*(brickInfo.width+brickInfo.padding))+brickInfo.offset.left;
			var brickY = (i*(brickInfo.height+brickInfo.padding))+brickInfo.offset.top;

			newBrick = game.add.sprite(brickX, brickY, 'brick');
            newBrick.life = 3-i;
            //console.log(newBrick.life);
			game.physics.enable(newBrick, Phaser.Physics.ARCADE);
			newBrick.body.immovable = true;
			newBrick.anchor.set(0.5);
            newBrick.tint = newBrick.life*0xffffff;
			bricks.add(newBrick);
		}
	}
}

function ballHitBrick(ball, brick) {
    if (brick.life <= 0) {
	var killTween = game.add.tween(brick.scale);
    killTween.to({x:0,y:0}, 600, Phaser.Easing.Linear.None);
    killTween.onComplete.addOnce(function() {
        brick.kill();
    }, this);
    killTween.start();
    }
	score += 10;
	scoreText.setText('Points: '+score);
	if(score === brickInfo.count.row*brickInfo.count.col*10) {
		alert('You won the game, yay you..');
		location.reload();
	}
}

function ballHitPaddle(ball, paddle) {
    ball.animations.play('wobble');
    ball.body.velocity.x = -1*5*(paddle.x-ball.x);
}

function ballLeaveScreen() {
    lives--;
    if(lives) {
        livesText.setText('Lives: '+lives);
        lifeLostText.visible = true;
        ball.reset(game.world.width*0.5, game.world.height-25);
        paddle.reset(game.world.width*0.5, game.world.height-5);
        game.input.onDown.addOnce(function() {
            lifeLostText.visible = false;
            ball.body.velocity.set(150, -150);
        }, this);
    }
    else {
        alert('You lost, game over!');
        location.reload();
    }
}

function startGame() {
    console.log("test");
    startButton.destroy();
    ball.body.velocity.set(150, -150);
    playing = true;
}